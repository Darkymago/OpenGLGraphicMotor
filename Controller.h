#pragma once

#include <math.h>

#include <SDL2/SDL.h>
#include <glm/glm.hpp>

class Controller {
  bool centerized = false;

  glm::vec3 position = glm::vec3(5,5,5);
  float hAngle =  M_PI*5.0f/4.0f;
  float vAngle = -M_PI/4.0f;
  float fFOV = 45.0f;

  float speed = 3.0f;
  float mouseSpeed = 0.005f;

  glm::mat4 view = glm::lookAt(
      glm::vec3(4,3,3),
      glm::vec3(0,0,0),
      glm::vec3(0,1,0));
  glm::mat4 projection = glm::perspective(45.0f,4.0f/3.0f,0.1f,100.0f);
  glm::mat4 model = glm::mat4(1.0f);

  bool processEvent(SDL_Event& e){
    Controller& m = *this;
    
    if (( e.type == SDL_QUIT ) || 
	((e.type == SDL_KEYDOWN) && (e.key.keysym.sym == SDLK_ESCAPE))
       ){
      return false;
    }

    glm::vec3 direction = glm::vec3(
	  std::cos(m.vAngle)*std::sin(m.hAngle),
	  std::sin(m.vAngle),
	  std::cos(m.vAngle)*std::cos(m.hAngle));
    glm::vec3 right = glm::vec3(
	std::sin(m.hAngle -M_PI/2.0f),
	0,
	std::cos(m.hAngle -M_PI/2.0f));
    glm::vec3 up = glm::cross(right, direction);

#define MIN_FOV 10.0f
#define MAX_FOV 180.0f
    switch (e.type){
      case SDL_MOUSEWHEEL:
	m.fFOV -= 5*e.wheel.y;
	m.fFOV  = std::fmod(fFOV, MAX_FOV) ? MIN_FOV : m.fFOV;
	break;
      case SDL_MOUSEMOTION:
	if (e.motion.state==1){
	  m.hAngle += (m.centerized ? -1 : 1) * m.mouseSpeed*e.motion.xrel;
	  m.vAngle += (m.centerized ? -1 : 1) * m.mouseSpeed*e.motion.yrel;
	}
	break;
      case SDL_KEYDOWN:
	switch (e.key.keysym.sym){
	  case SDLK_z:
	    m.position += direction*m.speed;
	    break;
	  case SDLK_s:
	    m.position -= direction*m.speed;
	    break;
	  case SDLK_d:
	    if (m.centerized)
	      m.hAngle -= m.speed/100;
	    else
	      m.position += right*m.speed;
	    break;
	  case SDLK_q:
	    if (m.centerized)
	      m.hAngle += m.speed/100;
	    else 
	      m.position -= right*m.speed;
	    break;
	  case SDLK_r:
	    m.position = glm::vec3(5,5,5);
	    m.hAngle =  M_PI*5.0f/4.0f;
	    m.vAngle = -M_PI/4.0f;
	    break;
	  case SDLK_SPACE:
	    m.centerized = !m.centerized;
	    if (m.centerized){
	      direction = -glm::normalize(position);
	      m.vAngle = std::asin(direction.y) ;
	      m.hAngle = (std::cos(m.vAngle)==0) ? std::acos(direction.x/std::cos(m.vAngle)) : m.hAngle;
	    }
	    break;
	}
    }
    m.hAngle = std::fmod(m.hAngle, 2*M_PI);
    m.vAngle = std::fmod(m.vAngle, 2*M_PI);
    m.position = m.centerized ?
      -glm::vec3(
	std::cos(m.vAngle)*std::sin(m.hAngle),
	std::sin(m.vAngle),
	std::cos(m.vAngle)*std::cos(m.hAngle)
      )*glm::length(m.position) :
      m.position;
    right = glm::vec3(
	std::sin(m.hAngle -M_PI/2.0f),
	0,
	std::cos(m.hAngle -M_PI/2.0f));
    up = glm::cross(right, direction);
    m.view = glm::lookAt(m.position, (m.centerized ? 0.0f : 1.0f)*(m.position+direction), up);
    m.projection = glm::perspective(m.fFOV, 4.0f/3.0f, 0.1f, 100.f);
    return true;
  };

public:
  bool computeEvents(){
    SDL_Event event;
    bool bContinue = true;
    while (bContinue && SDL_PollEvent(&event)){
      bContinue = processEvent(event);
    }
    return bContinue;
  };

  const glm::mat4& getModelMatrix(){return this->model;};
  const glm::mat4& getViewMatrix(){return this->view;};
  const glm::mat4& getProjectionMatrix(){return this->projection;};
};
