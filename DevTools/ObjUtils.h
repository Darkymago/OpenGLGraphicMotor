#ifndef OBJUTILS_H
#define OBJUTILS_H

#include <vector>
#include <stdio.h>
#include <string.h>

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "TextureUtils.h"

typedef glm::vec3 v3;
typedef glm::vec2 v2;

typedef std::vector<v3> tv3;
typedef std::vector<v2> tv2;

/************************************************************************
 * Load OBJ file 
 * Params
 *  - path : path of OBJ
 *  - outVertices : vector in which loaded vertices will be appended
 *  - outUVs : vector in which loaded UV mappings will be appended
 *  - outNormals : vector in which loaded normals will be appended
 * Return
 *  - number of loaded objects (vertices/uvs/normals have same count)
 ************************************************************************/
const int loadOBJ(
  const char * path,
  tv3& outVertices,
  tv2& outUVs,
  tv3& outNormals){

  std::vector<unsigned int> vuiVertex, vuiUV, vuiNormal;
  tv3 tempVertices;
  tv2 tempUVs;
  tv3 tempNormals;

  FILE* pFile = fopen(path, "r");
  if (!pFile){
    printf("File %s not found !\n", path);
    getchar();
    return false;
  }

  while(1){
    char lineHeader[128];
    int res = fscanf(pFile, "%s", lineHeader);
    if (res==EOF)
      break;

    // #ToDo: Replace with CRC32 switch
    if (strcmp(lineHeader, "v") == 0){
      glm::vec3 vertex;
      fscanf(pFile, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
      tempVertices.push_back(vertex);
    }
    else if (strcmp(lineHeader, "vt") == 0){
      glm::vec2 uv;
      fscanf(pFile, "%f %f\n", &uv.x, &uv.y);
      tempUVs.push_back(uv);
    }
    else if (strcmp(lineHeader, "vn") == 0){
      glm::vec3 normal;
      fscanf(pFile, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
      tempNormals.push_back(normal);
    }
    else if (strcmp(lineHeader, "f") == 0){
      unsigned int uiVertex[3], uiUV[3], uiNormal[3];
      int iMatches = fscanf(pFile, "%d/%d/%d %d/%d/%d %d/%d/%d\n",
	  &uiVertex[0], &uiUV[0], &uiNormal[0],
	  &uiVertex[1], &uiUV[1], &uiNormal[1],
	  &uiVertex[2], &uiUV[2], &uiNormal[2]);
      if (iMatches!=9){
	printf("Necessite other obj parser !\n");
	return false;
      }

      vuiVertex.push_back(uiVertex[0]);
      vuiVertex.push_back(uiVertex[1]);
      vuiVertex.push_back(uiVertex[2]);
      vuiUV.push_back(uiUV[0]);
      vuiUV.push_back(uiUV[1]);
      vuiUV.push_back(uiUV[2]);
      vuiNormal.push_back(uiNormal[0]);
      vuiNormal.push_back(uiNormal[1]);
      vuiNormal.push_back(uiNormal[2]);
    }
    else {
      char dumpBuffer[1000];
      fgets(dumpBuffer, 1000, pFile);
    }
  }

  for (unsigned int i=0; i<vuiVertex.size(); ++i){
    unsigned int uiVertex = vuiVertex[i];
    unsigned int uiUV = vuiUV[i];
    unsigned int uiNormal = vuiNormal[i];

    v3 vertex = tempVertices[uiVertex-1];
    v2 uv = tempUVs[uiUV-1];
    v3 normal = tempNormals[uiNormal-1];

    outVertices.push_back(vertex);
    outUVs.push_back(uv);
    outNormals.push_back(normal);
  }

  return vuiVertex.size();
};

/* *********************************************************** * 
 * CRTP base class for all 3D Drawn Material
 * contains all vertices and normals of a given derived class
 * *********************************************************** */
template<typename T>
class ObjectBase {
protected:
  static int iNext;
  static tv3 tVertices;
  static tv3 tNormals;
public:
  v3 vTranslation = {0.0f, 0.0f, 0.0f};
  v3 vScaling = {1.0f, 1.0f, 1.0f};
  v3 vRotation = {0.0f, 0.0f, 0.0f};

public:
  void translate(v3 translation){ vTranslation = translation; };
  void scale(v3 scaling){ vScaling = scaling; };
  void rotate(v3 rotation){ vRotation = rotation; };

  const glm::mat4 getModelView(const glm::mat4& cameraModelView) const {
    glm::mat4 mCurrent = glm::translate(cameraModelView, vTranslation);
    mCurrent = glm::scale(mCurrent, vScaling);
    mCurrent = glm::rotate(mCurrent, vRotation.x, v3(1.0f, 0.0f, 0.0f));
    mCurrent = glm::rotate(mCurrent, vRotation.y, v3(0.0f, 1.0f, 0.0f));
    mCurrent = glm::rotate(mCurrent, vRotation.z, v3(0.0f, 0.0f, 1.0f));
    return mCurrent;
  }

  static int size(){ return tVertices.size(); };
  static v3* pVOrigin(){ return &(tVertices[0]); };
  static v3* pNOrigin(){ return &(tNormals[0]); };
};
template<typename T> int ObjectBase<T>::iNext = 0;
template<typename T> tv3 ObjectBase<T>::tVertices;
template<typename T> tv3 ObjectBase<T>::tNormals;

template<typename T>
class TexturedObjectBase : public ObjectBase<T> {
protected:
  static tv2 tUVs;
  Texture const * pTexture;
public:
  static v2* pUVOrigin(){ return &tUVs[0]; };
  const Texture& getTexture() const { return *pTexture; };
  void setTexture(Texture& texture) { pTexture = &texture; };
};
template<typename T> tv2 TexturedObjectBase<T>::tUVs;

class ColoredObject : public ObjectBase<ColoredObject> {
  const int iStart, nVertices;
  v3 vColor;

  ColoredObject(tv3 Vertices, tv3 Normals, v3 color) : iStart(iNext), nVertices(Vertices.size()) {
    tVertices.insert(tVertices.end(), Vertices.begin(), Vertices.end());
    tNormals.insert(tNormals.end(), Normals.begin(), Normals.end());
    iNext += nVertices;
  };
  ColoredObject(const char* path) : iStart(iNext), nVertices(loadOBJ(path, tVertices, *new tv2(), tNormals)){
    iNext += nVertices;
  };
};

class TexturedObject : public TexturedObjectBase<TexturedObject> {
  const int iStart, nVertices;
  
public:
  TexturedObject(tv3 Vertices, tv2 UVs, tv3 Normals) : iStart(iNext), nVertices(Vertices.size()) {
    tVertices.insert(tVertices.end(), Vertices.begin(), Vertices.end());
    tUVs.insert(tUVs.end(), UVs.begin(), UVs.end());
    tNormals.insert(tNormals.end(), Normals.begin(), Normals.end());
    iNext += nVertices;
  };
  TexturedObject(const char* path) : iStart(iNext), nVertices(loadOBJ(path, tVertices, tUVs, tNormals)) {
    iNext += nVertices;
  };

  const int getStart() const { return iStart; };
  const int getSize() const { return nVertices; };
};

#endif // OBJUTILS_H
