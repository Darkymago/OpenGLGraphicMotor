#ifndef SHADERUTILS_H
#define SHADERUTILS_H

#include <fstream>
#include <sstream>
#include <vector>

#include <GL/gl.h>

class ShaderBase {
protected:
  GLuint uiProgram;

public:
  virtual void draw() = 0;
  virtual void init() = 0;
  void LoadShaders(const std::string &sVertPath, const std::string &sFragPath){
    ShaderBase& m = *this;
    GLint iRes = GL_FALSE;
    m.uiProgram = glCreateProgram();

    GLuint uiVertShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint uiFragShader = glCreateShader(GL_FRAGMENT_SHADER);
    if (CompileShader(sVertPath, uiVertShader) && CompileShader(sFragPath, uiFragShader)){
      glAttachShader(m.uiProgram, uiVertShader);
      glAttachShader(m.uiProgram, uiFragShader);
      glLinkProgram(m.uiProgram);
      iRes = CheckOpStatus<glGetProgramiv>(uiProgram, GL_LINK_STATUS, "Program Link");
    }

    if (!iRes){
      printf("Something went wrong!\n");
    }

    glDeleteShader(uiVertShader);
    glDeleteShader(uiFragShader);
  }

protected:
  static std::string FileToString(const std::string &fPath){
    std::ifstream fileStream(fPath.c_str());
    if (!fileStream){
      printf("/!\\ Error Reading Shader '%s'\n", fPath.c_str());
      return "";
    }

    std::stringstream sstr;
    sstr << fileStream.rdbuf();
    return sstr.str();
  };

  template<void (*glGetTemplatev)(GLuint, GLenum, GLint*)>
  static GLint CheckOpStatus(GLuint uiTemplate, GLenum eStatToCheck, const char *sTemplateCheck){
    GLint iRes = GL_FALSE;
    int iInfoLogLength(0);
    glGetTemplatev(uiTemplate, eStatToCheck, &iRes);
    glGetTemplatev(uiTemplate, GL_INFO_LOG_LENGTH, &iInfoLogLength);
    if (!iRes && iInfoLogLength > 0) {
      std::vector<char> ErrorMsg(iInfoLogLength+1);
      glGetShaderInfoLog(uiTemplate, iInfoLogLength, NULL, &ErrorMsg[0]);
      printf("%s Error : %s\n", sTemplateCheck, &ErrorMsg[0]);
    }
    return iRes;
  }

  static GLint CompileShader(const std::string &ShaderPath, const GLuint &uiShader){
    const std::string& ShaderString(FileToString(ShaderPath));
    char const *pSource = ShaderString.c_str();
    GLint iShaderLength = (GLint) ShaderString.size();
    glShaderSource(uiShader, 1, (const GLchar**)&pSource, &iShaderLength);
    glCompileShader(uiShader);
    //to change with CheckShaderOperationStatus
    return CheckOpStatus<glGetShaderiv>(uiShader, GL_COMPILE_STATUS, ("Shader Compilation " + ShaderPath).c_str());
  };

  template<void (*glUniformTemplate)(GLuint, GLuint, void*)>
  void setUniformFromName(std::string sUniName, void* values, int iCount=1){
    int iLoc = glGetUniformLocation(uiProgram, sUniName.c_str());
    glUniformTemplate(iLoc, iCount, values);
  };

  template<void (*glUniformTemplate)(GLuint, GLuint, void*)>
  void setUniformFromLocation(int iLoc, void* values, int iCount=1){
    glUniformTemplate(iLoc, iCount, values);
  };
};

#endif //SHADERUTILS_H
