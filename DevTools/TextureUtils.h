#ifndef TEXTUREUTILS_H
#define TEXTUREUTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

#include <FreeImage.h>

class Texture {
  int iWidth, iHeight, iBPP;
  GLuint uiTexture;
  GLuint uiSampler;
  bool bMipMapsGenerated;

  int tfMinification, tfMagnification;

  std::string sPath;

public:
  enum TextureFiltering {
    TEXTURE_FILTER_MAG_NEAREST = 0,	// Nearest for Mag
    TEXTURE_FILTER_MAG_BILINEAR,	// Bilinear for Mag
    TEXTURE_FILTER_MIN_NEAREST,		// Nearest for Min
    TEXTURE_FILTER_MIN_BILINEAR,	// Bilinear for Min
    TEXTURE_FILTER_MIN_NEAREST_MIPMAP,	// Nearest for Min, Closest on MipMap
    TEXTURE_FILTER_MIN_BILINEAR_MIPMAP,	// Bilinear for Min, Closest on MipMap
    TEXTURE_FILTER_MIN_TRILINEAR,	// Avg of Bilin for Min on 2 Closest MipMap
  };

  bool loadTexture2D(std::string a_sPath, bool bGenerateMipMaps = false){
    printf("Loading texture\n");
    Texture& m = *this;
    FREE_IMAGE_FORMAT fiformat = FIF_UNKNOWN;
    FIBITMAP* pImageBM(0);

    m.sPath = a_sPath;
    fiformat = FreeImage_GetFileType(m.sPath.c_str(),0);
    if (fiformat == FIF_UNKNOWN){ fiformat = FreeImage_GetFIFFromFilename(m.sPath.c_str()); }
    if (fiformat == FIF_UNKNOWN){ return false; }

    if (FreeImage_FIFSupportsReading(fiformat)){ pImageBM = FreeImage_Load(fiformat, m.sPath.c_str()); }
    if (!pImageBM) { return false; }

    m.iWidth = FreeImage_GetWidth(pImageBM);
    m.iHeight = FreeImage_GetHeight(pImageBM);
    m.iBPP = FreeImage_GetBPP(pImageBM);
    printf("Size of Image Loaded is: %i, %i\n", m.iWidth, m.iHeight);

    glGenTextures(1, &m.uiTexture);
    glBindTexture(GL_TEXTURE_2D, m.uiTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m.iWidth, m.iHeight, 0, GL_BGRA, GL_UNSIGNED_BYTE, (GLvoid*)FreeImage_GetBits(pImageBM));
    FreeImage_Unload(pImageBM);

    if (bGenerateMipMaps){
      glGenerateMipmap(GL_TEXTURE_2D);
      m.bMipMapsGenerated = bGenerateMipMaps;
    }

    glGenSamplers(1, &uiSampler);

    return true;
  };

  void bindTexture(unsigned int uiTextureUnit = 0) const {
    const Texture& m = *this;
    glActiveTexture(GL_TEXTURE0+uiTextureUnit);
    glBindTexture(GL_TEXTURE_2D, m.uiTexture);
    glBindSampler(uiTextureUnit, m.uiSampler);
  };

  void setFiltering(int a_tfMagnification, int a_tfMinification){
    Texture& m = *this;

    switch (a_tfMinification){
      case TextureFiltering::TEXTURE_FILTER_MAG_NEAREST:
	glSamplerParameteri(uiSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	break;
      case TextureFiltering::TEXTURE_FILTER_MAG_BILINEAR:
	glSamplerParameteri(uiSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	break;
    }

    switch (a_tfMagnification){
      case TextureFiltering::TEXTURE_FILTER_MIN_NEAREST:
	glSamplerParameteri(uiSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	break;
      case TextureFiltering::TEXTURE_FILTER_MIN_BILINEAR:
	glSamplerParameteri(uiSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	break;
      case TextureFiltering::TEXTURE_FILTER_MIN_NEAREST_MIPMAP:
	glSamplerParameteri(uiSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	break;
      case TextureFiltering::TEXTURE_FILTER_MIN_BILINEAR_MIPMAP:
	glSamplerParameteri(uiSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	break;
      case TextureFiltering::TEXTURE_FILTER_MIN_TRILINEAR:
	glSamplerParameteri(uiSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	break;
    }

    m.tfMinification = a_tfMinification;
    m.tfMagnification = a_tfMagnification;
  };

  int getMinificationFilter(){ return this->tfMinification; };
  int getMagnificationFilter(){ return this->tfMagnification; };

  void releaseTexture(){
    glDeleteSamplers(1, &uiSampler);
    glDeleteTextures(1, &uiTexture);
  };

  // --- accessors ---
  const GLuint get_uiTexture() const { return uiTexture; };
  
  Texture(){
    Texture& m = *this;
    m.bMipMapsGenerated = false;
  };
};

#endif //TEXTUREUTILS_H
