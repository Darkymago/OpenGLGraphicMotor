#include "ShaderUtils.h"

#define SIMPLE_SHADER_VERT_FILE "Shaders/SimpleShader.vert.glsl"
#define SIMPLE_SHADER_FRAG_FILE "Shaders/SimpleShader.frag.glsl"

class SimpleShader : public ShaderBase {
  GLuint uiVertexArray;
  GLuint uiVertexBuffer;

  enum {arrsize = 9};
  const GLfloat g_vertex_buffer_data[arrsize] = {
    -1.0f, -1.0f, 0.0f,
     1.0f, -1.0f, 0.0f,
     0.0f,  1.0f, 0.0f
  };

public:
  virtual void init() {
    SimpleShader& m = *this;
    glGenVertexArrays(1, &m.uiVertexArray);
    glBindVertexArray(m.uiVertexArray);

    m.LoadShaders(SIMPLE_SHADER_VERT_FILE, SIMPLE_SHADER_FRAG_FILE);
    glGenBuffers(1, &m.uiVertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(m.g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

  };
  virtual void draw() {
    SimpleShader& m = *this;
    glUseProgram(m.uiProgram);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiVertexBuffer);
    glVertexAttribPointer(
	0,
	3,
	GL_FLOAT,
	GL_FALSE,
	0,
	(void*)0
    );
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDisableVertexAttribArray(0);

  };
};
