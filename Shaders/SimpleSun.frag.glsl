#version 330 core

in vec2 frag_UV;
in vec3 frag_Normal;

out vec3 color;

uniform sampler2D myTextureSampler;

struct SimpleSun {
  vec3 vColor;
  vec3 vDirection;
  float fAmbiantIntensity;
};

uniform SimpleSun sunLight;

void main(){
  vec3 vTexColor = texture2D(myTextureSampler, frag_UV).rgb;
  float fDiffuseIntensity = max(0.0, dot(normalize(frag_Normal), -sunLight.vDirection));
  color = vTexColor*sunLight.vColor*(sunLight.fAmbiantIntensity+fDiffuseIntensity);
}
