#pragma once

#include "../DevTools/ShaderUtils.h"
#include "../DevTools/TextureUtils.h"
#include "../DevTools/ObjUtils.h"
#include "../World/WorldStructures.h"

#define TEXTURE_SHADER_VERT_FILE "Shaders/Texture.vert.glsl"
#define TEXTURE_SHADER_FRAG_FILE "Shaders/SimpleSun.frag.glsl"

class SimpleSunShader : public ShaderBase {
  GLuint uiVertexArray;
  GLuint uiVertexBuffer;
  GLuint uiUVBuffer;
  GLuint uiNormalBuffer;

  struct {
    // Uniforms
    GLuint uiMVP;
    GLuint uiSunColor;
    GLuint uiSunPolCoord;
    GLuint uiSunAmbiantIntensity;
    // Locals
    GLuint uiPos = 0;
    GLuint uiUV = 1;
    GLuint uiNormal = 2;
  } locations;

  std::vector<TexturedObject> objects;
  std::vector<Texture> textures;

  SimpleSun simpleSun {
    {1.0f, 1.0f, 1.0f},
    {-3.14f/4, 0.0f},
    0.2f
  };

  const glm::mat4* pModel;
  const glm::mat4* pView;
  const glm::mat4* pProjection;

public:
  virtual void init() {
    printf("Shader Init\n");
    SimpleSunShader& m = *this;

    glGenVertexArrays(1, &m.uiVertexArray);
    glBindVertexArray(m.uiVertexArray);

    m.LoadShaders(TEXTURE_SHADER_VERT_FILE, TEXTURE_SHADER_FRAG_FILE);

    m.locations.uiMVP = glGetUniformLocation(m.uiProgram, "MVP");
    m.locations.uiSunColor = glGetUniformLocation(m.uiProgram, "sunLight.vColor");
    m.locations.uiSunPolCoord = glGetUniformLocation(m.uiProgram, "sunLight.vDirection");
    m.locations.uiSunAmbiantIntensity = glGetUniformLocation(m.uiProgram, "sunLight.fAmbiantIntensity");

    glEnable(GL_TEXTURE_2D);

    int nVertices = TexturedObject::size();
    glGenBuffers(1, &m.uiVertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, nVertices*sizeof(glm::vec3), TexturedObject::pVOrigin(), GL_STATIC_DRAW);

    glGenBuffers(1, &m.uiUVBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiUVBuffer);
    glBufferData(GL_ARRAY_BUFFER, nVertices*sizeof(glm::vec3), TexturedObject::pUVOrigin(), GL_STATIC_DRAW);

    glGenBuffers(1, &m.uiNormalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, nVertices*sizeof(glm::vec3), TexturedObject::pNOrigin(), GL_STATIC_DRAW);

    printf("Shader Init End\n");
  };
  virtual ~SimpleSunShader(){
    SimpleSunShader& m = *this;

    glDeleteBuffers(1, &m.uiVertexBuffer);
    glDeleteBuffers(1, &m.uiUVBuffer);
    glDeleteBuffers(1, &m.uiNormalBuffer);
    for(unsigned int iTex(0); iTex < textures.size(); ++iTex) { textures[iTex].releaseTexture(); }
    glDeleteProgram(m.uiProgram);
    glDeleteVertexArrays(1, &m.uiVertexArray);
  };
  virtual void draw() {
    SimpleSunShader& m = *this;
    glUseProgram(m.uiProgram);

    glUniform3fv(m.locations.uiSunColor, 1, &simpleSun.vColor[0]);
    glUniform3fv(m.locations.uiSunPolCoord, 1, &glm::euclidean(simpleSun.vPolCoord)[0]);
    glUniform1fv(m.locations.uiSunAmbiantIntensity, 1, &simpleSun.fAmbiantIntensity);

    glEnableVertexAttribArray(m.locations.uiPos);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiVertexBuffer);
    glVertexAttribPointer(
	m.locations.uiPos,
	3,
	GL_FLOAT,
	GL_FALSE,
	0,
	(void*)0
    );

    glEnableVertexAttribArray(m.locations.uiUV);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiUVBuffer);
    glVertexAttribPointer(
	m.locations.uiUV,
	2,
	GL_FLOAT,
	GL_FALSE,
	0,
	(void*)0
    );

    glEnableVertexAttribArray(m.locations.uiNormal);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiNormalBuffer);
    glVertexAttribPointer(
	m.locations.uiNormal,
	3,
	GL_FLOAT,
	GL_FALSE,
	0,
	(void*)0
    );

    for (unsigned int i=0; i<objects.size(); ++i){
      const TexturedObject& object = (m.objects)[i];
      const Texture& tex = object.getTexture();
      const glm::mat4 currentModelView = object.getModelView(*m.pModel * *m.pView);
      glUniform1i(tex.get_uiTexture(), 0);
      tex.bindTexture(0);
      glUniformMatrix4fv(m.locations.uiMVP, 1, GL_FALSE, &(*m.pProjection * currentModelView)[0][0]);
      glDrawArrays(GL_TRIANGLES, object.getStart(), object.getSize());
    }

    glDisableVertexAttribArray(m.locations.uiPos);
    glDisableVertexAttribArray(m.locations.uiUV);
    glDisableVertexAttribArray(m.locations.uiNormal);
  };

  TexturedObject& addObject(std::string sPath){ 
    TexturedObject res = *new TexturedObject(sPath.c_str());
    objects.push_back(res);
    return objects.back();
  };
  TexturedObject& addObject(
      std::vector<glm::vec3> objVertices,
      std::vector<glm::vec2> objUVs,
      std::vector<glm::vec3> objNormals){ 
    TexturedObject res = *new TexturedObject(objVertices, objUVs, objNormals);
    objects.push_back(res);
    return objects.back();
  };

  void setpMVP(const glm::mat4& nModel, const glm::mat4& nView, const glm::mat4& nProjection){ 
    this->pModel = &nModel; 
    this->pView = &nView; 
    this->pProjection = &nProjection;
  };
};
