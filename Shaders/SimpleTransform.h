#include "ShaderUtils.h"

#define SIMPLETRANSFORM_SHADER_VERT_FILE "Shaders/SimpleTransform.vert.glsl"
#define SIMPLETRANSFORM_SHADER_FRAG_FILE "Shaders/SimpleShader.frag.glsl"

class SimpleTransform : public ShaderBase {
  GLuint uiVertexArray;
  GLuint uiVertexBuffer;

  struct {
    GLuint uiMVP;
  } locations;

  const GLfloat * pMVP;

  enum {arrsize = 9};
  const GLfloat g_vertex_buffer_data[arrsize] = {
    -1.0f, -1.0f, 0.0f,
     1.0f, -1.0f, 0.0f,
     0.0f,  1.0f, 0.0f
  };

public:
  virtual void init() {
    SimpleTransform& m = *this;
    glGenVertexArrays(1, &m.uiVertexArray);
    glBindVertexArray(m.uiVertexArray);

    m.LoadShaders(SIMPLETRANSFORM_SHADER_VERT_FILE, SIMPLETRANSFORM_SHADER_FRAG_FILE);
    m.locations.uiMVP = glGetUniformLocation(m.uiProgram, "MVP");

    glGenBuffers(1, &m.uiVertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(m.g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
  };
  virtual void draw() {
    SimpleTransform& m = *this;
    glUseProgram(m.uiProgram);
    glUniformMatrix4fv(m.locations.uiMVP, 1, GL_FALSE, pMVP);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiVertexBuffer);
    glVertexAttribPointer(
	0,
	3,
	GL_FLOAT,
	GL_FALSE,
	0,
	(void*)0
    );
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDisableVertexAttribArray(0);
  };

  void setpMVP(const GLfloat * nMVP){ this->pMVP = nMVP; };
};
