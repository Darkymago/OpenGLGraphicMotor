#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vPosModelspace;
layout(location = 1) in vec2 vUV;
layout(location = 2) in vec3 vNormal;

out vec2 frag_UV;
out vec3 frag_Normal;

uniform mat4 MVP;

void main(){

    gl_Position = MVP * vec4(vPosModelspace, 1);

    frag_UV = vUV;
    frag_Normal = vNormal;

}

