#include "ShaderUtils.h"
#include "../Textures/TextureUtils.h"
#include "../World/ObjLoader.h"

#define TEXTURE_SHADER_VERT_FILE "Shaders/Texture.vert.glsl"
#define TEXTURE_SHADER_FRAG_FILE "Shaders/Texture.frag.glsl"

#define TEXTURE_DDS_FILE "/run/media/darkymago/Donnees/Dev/OpenGLGraphicMotor/Textures/uvmap.DDS"
#define OBJ_CUBE_FILE "World/cube.obj"

class TextureShader : public ShaderBase {
  GLuint uiVertexArray;
  GLuint uiVertexBuffer;
  GLuint uiUVBuffer;

  struct {
    GLuint uiMVP;
    GLuint uiPos = 0;
    GLuint uiUV = 1;
  } locations;

  Texture tex;
  GLuint uiTextureDDS;
  GLuint uiTexture;

  const GLfloat * pMVP;

  std::vector<glm::vec3> vertices;
  std::vector<glm::vec2> uvs;
  std::vector<glm::vec3> normals;

public:
  virtual void init() {
    printf("Shader Init\n");
    TextureShader& m = *this;
    glGenVertexArrays(1, &m.uiVertexArray);
    glBindVertexArray(m.uiVertexArray);

    m.LoadShaders(TEXTURE_SHADER_VERT_FILE, TEXTURE_SHADER_FRAG_FILE);
    m.locations.uiMVP = glGetUniformLocation(m.uiProgram, "MVP");

    //m.uiTextureDDS = loadDDS(TEXTURE_DDS_FILE);
    //m.uiTexture = glGetUniformLocation(m.uiProgram, "myTextureSampler");
    m.tex.loadTexture2D(TEXTURE_DDS_FILE, true);
    m.tex.setFiltering(Texture::TextureFiltering::TEXTURE_FILTER_MAG_BILINEAR, Texture::TextureFiltering::TEXTURE_FILTER_MIN_BILINEAR_MIPMAP);
    glEnable(GL_TEXTURE_2D);

    loadOBJ(OBJ_CUBE_FILE, m.vertices, m.uvs, m.normals);

    glGenBuffers(1, &m.uiVertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, m.vertices.size()*sizeof(glm::vec3), &m.vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &m.uiUVBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiUVBuffer);
    glBufferData(GL_ARRAY_BUFFER, m.uvs.size()*sizeof(glm::vec2), &m.uvs[0], GL_STATIC_DRAW);
    printf("Shader Init End\n");
  };
  virtual ~TextureShader(){
    TextureShader& m = *this;

    glDeleteBuffers(1, &m.uiVertexBuffer);
    glDeleteBuffers(1, &m.uiUVBuffer);
    glDeleteTextures(1, &m.uiTexture);
    m.tex.releaseTexture();
    glDeleteProgram(m.uiProgram);
    glDeleteVertexArrays(1, &m.uiVertexArray);
  };
  virtual void draw() {
    TextureShader& m = *this;
    glUseProgram(m.uiProgram);

    glUniformMatrix4fv(m.locations.uiMVP, 1, GL_FALSE, pMVP);

    //glActiveTexture(GL_TEXTURE0);
    //glBindTexture(GL_TEXTURE_2D, uiTextureDDS);
    glUniform1i(m.tex.get_uiTexture(), 0);
    m.tex.bindTexture(0);

    glEnableVertexAttribArray(m.locations.uiPos);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiVertexBuffer);
    glVertexAttribPointer(
	m.locations.uiPos,
	3,
	GL_FLOAT,
	GL_FALSE,
	0,
	(void*)0
    );

    glEnableVertexAttribArray(m.locations.uiUV);
    glBindBuffer(GL_ARRAY_BUFFER, m.uiUVBuffer);
    glVertexAttribPointer(
	m.locations.uiUV,
	2,
	GL_FLOAT,
	GL_FALSE,
	0,
	(void*)0
    );
    glDrawArrays(GL_TRIANGLES, 0, 12*3);
    glDisableVertexAttribArray(m.locations.uiPos);
    glDisableVertexAttribArray(m.locations.uiUV);
  };

  void setpMVP(const GLfloat * nMVP){ this->pMVP = nMVP; };
};
