#pragma once

#include <SDL2/SDL.h>
#include <GL/gl.h>

#include "World/World.h"
#include "Controller.h"
#include "Shaders/SimpleSunShader.h"

class Viewer {
  bool bReady;

  int iHeight;
  int iWidth;

  SDL_Window* pWindow;
  SDL_GLContext gl;

  World world;
  Controller controller;

public:
  Viewer(){
    Viewer& m = *this;
    m.iWidth = 640;
    m.iHeight = 580;
    m.bReady = false;

    if (SDL_Init(SDL_INIT_VIDEO)!=0){
      printf("Fail to initialize SDL : %s\n", SDL_GetError());
      return;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // Double Buffering
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    m.pWindow= SDL_CreateWindow("First Window",
	SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
	m.iWidth, m.iHeight,
	SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

    m.gl = SDL_GL_CreateContext(pWindow);
    if (m.gl==NULL){
      printf("OpenGL context not created : %s\n", SDL_GetError());
      return;
    }

    SDL_GL_SetSwapInterval(1);
    glClearColor(0.1f, 0.1f, 0.1f, 0.0f); //RGBA

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // #ToDo : Shader Structure
    m.world.init();
    m.world.setController(controller);
    m.bReady = true;
  };
  ~Viewer(){
    printf("Destroying Context and al.\n");
    SDL_GL_DeleteContext(gl);
    SDL_DestroyWindow(pWindow);
    SDL_Quit();
  }

  int Run(){
    Viewer& m = *this;

    if (!m.bReady){
      return -1;
    }
    
    do {
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      m.world.timelapse();
      m.world.draw();
      SDL_GL_SwapWindow(m.pWindow);
    } while (m.controller.computeEvents());
    return 1;
  }
};
