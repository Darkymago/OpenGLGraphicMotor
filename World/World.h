#pragma once

#include <ctime>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../DevTools/ObjUtils.h"
#include "../DevTools/TextureUtils.h"

#include "../Controller.h"
#include "WorldStructures.h"
#include "../Shaders/SimpleSunShader.h"

#define GROUND_IMG_FILE "Textures/ground.jpg"
#define SUZANNE_DDS_FILE "Textures/suzanne.dds"
#define SUZANNE_OBJ_FILE "World/suzanne.obj"

class World {
  glm::mat4 ModelView;
  glm::mat4 Projection;

  SimpleSunShader simpleSunShader;
  Texture groundTexture;
  Texture suzTexture;

  SimpleSun oSun {
    {1.0f, 1.0f, 1.0f},
    {-3.14f/4, 0.0f},
    0.2f
  };
  time_t worldTime = std::time(nullptr);

  Controller* pController;

public:
  void init(){

    std::vector<glm::vec3> suzanneVertices;
    std::vector<glm::vec2> suzanneUVs;
    std::vector<glm::vec3> suzanneNormals;
    loadOBJ(SUZANNE_OBJ_FILE, suzanneVertices, suzanneUVs, suzanneNormals);

    std::vector<glm::vec3> floorVertices {
      {-100.0f, 0.0f,-100.0f},
      {-100.0f, 0.0f, 100.0f},
      { 100.0f, 0.0f,-100.0f},
      {-100.0f, 0.0f, 100.0f},
      { 100.0f, 0.0f,-100.0f},
      { 100.0f, 0.0f, 100.0f}
    };
    std::vector<glm::vec2> floorUVs {
      { 0.0f,  0.0f},
      { 0.0f,  10.0f},
      { 10.0f,  0.0f},
      { 0.0f,  10.0f},
      { 10.0f,  0.0f},
      { 10.0f,  10.0f}
    };
    std::vector<glm::vec3> floorNormals {
      { 0.0f, 1.0f, 0.0f},
      { 0.0f, 1.0f, 0.0f},
      { 0.0f, 1.0f, 0.0f},
      { 0.0f, 1.0f, 0.0f},
      { 0.0f, 1.0f, 0.0f},
      { 0.0f, 1.0f, 0.0f}
    };
    
    groundTexture.loadTexture2D(GROUND_IMG_FILE, true);
    groundTexture.setFiltering(Texture::TextureFiltering::TEXTURE_FILTER_MAG_BILINEAR, Texture::TextureFiltering::TEXTURE_FILTER_MIN_BILINEAR_MIPMAP);

    TexturedObject& groundObj = simpleSunShader.addObject(floorVertices, floorUVs, floorNormals);
    groundObj.setTexture(groundTexture);
    
    suzTexture.loadTexture2D(SUZANNE_DDS_FILE, true);
    suzTexture.setFiltering(Texture::TextureFiltering::TEXTURE_FILTER_MAG_BILINEAR, Texture::TextureFiltering::TEXTURE_FILTER_MIN_BILINEAR_MIPMAP);
    float squareHalfLength = 15.0f;
    for (float x(-squareHalfLength); x <= squareHalfLength; x+=3.0f){
      for (float y(3.0f); y <= squareHalfLength; y+=3.0f){
	for (float z(-squareHalfLength); z <= squareHalfLength; z+=3.0f){
	  TexturedObject& suz = simpleSunShader.addObject(suzanneVertices, suzanneUVs, suzanneNormals);
	  suz.translate(glm::vec3(x, y, z));
	  suz.setTexture(suzTexture);
	}
      }
    }
    simpleSunShader.init();
  };
  void setController(Controller& a_controller){ pController = &a_controller; };

  void draw(){
    simpleSunShader.setpMVP(pController->getModelMatrix(), pController->getViewMatrix(), pController->getProjectionMatrix());
    simpleSunShader.draw();
  };
//  void setMVP(
//      const glm::mat4 & model,
//      const glm::mat4 & view,
//      const glm::mat4 & projection){
//    ModelView = view * model;
//    Projection = projection;
//  };
  void timelapse(int nSec=1){
    worldTime += nSec;
    //oSun.setPolCoord(2*3.14*(worldTime%100)/100.0f);
  };
  const SimpleSun* getpSun() { return &oSun; };

};
