#ifndef WORLDSTRUCT_H
#define WORLDSTRUCT_H

#include <glm/glm.hpp>
#include <glm/gtx/polar_coordinates.hpp>

struct SimpleSun {
  glm::vec3 vColor;
  glm::vec2 vPolCoord;
  float fAmbiantIntensity;

  void setPolCoord(const float phi, const float psi=0){ vPolCoord.x = phi; vPolCoord.y = psi; }
};

#endif // WORLDSTRUCT_H
