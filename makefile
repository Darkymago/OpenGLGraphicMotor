CC=gcc
GLSDL_CFLAGS=-lGL -lSDL2 -DGL_GLEXT_PROTOTYPES -lfreeimage
CFLAGS=$(GLSDL_CFLAGS) -Wall -lstdc++ -std=c++11 -lm
DEBUG_FLD=./build/debug/
RELEASE_FLD=./build/release/

release: main.cpp
	$(CC) -o $(RELEASE_FLD)/motor main.cpp $(CFLAGS)

debug: main.cpp
	$(CC) -g -o $(DEBUG_FLD)/motor_dbg main.cpp $(CFLAGS)

lauch_rel: release
	$(RELEASE_FLD)/motor

lauch_dbg: debug
	$(DEBUG_FLD)/motor_dbg
